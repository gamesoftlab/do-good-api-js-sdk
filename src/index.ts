import { GnSdk } from './classes/GnSdk';

window.gn = new GnSdk();

console.log(
    window.gn.createWidget('body', {
        theme: 'dark',
        orderHash: '1234wdawd',
        lang: 'en',
    }),
);
