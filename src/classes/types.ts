export interface IGnSdkConfig {
    orderHash: string;
    lang: string;
    theme: string;
}
