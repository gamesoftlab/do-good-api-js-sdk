import { WIDGET_ID } from './constants';
import { IGnSdkConfig } from './types';
import { isNil } from '../utils';

export class GnSdk {
    createWidget(selector: string, config: IGnSdkConfig) {
        const mountElement = document.querySelector(selector);

        if (isNil(mountElement)) {
            throw new Error(`Can not find element by given selector (${selector})`);
        }

        const element = document.createElement('iframe');

        element.setAttribute('id', WIDGET_ID);
        element.setAttribute('data-config', JSON.stringify(config));

        mountElement.appendChild(element);

        return {
            changeTheme: (theme: string) => {
                console.log('changeTheme');
            },
            changeLang: (lang: string) => {
                document.getElementById(WIDGET_ID)!.setAttribute('data-lang', config.lang);
            },
            changeOrderHash: (orderHash: string) => {
                document.getElementById(WIDGET_ID)!.setAttribute('data-orderhash', config.orderHash);
            },
        };
    }

    private getWidgetElement(): HTMLIFrameElement {
        return document.getElementById(WIDGET_ID) as HTMLIFrameElement;
    }
}
