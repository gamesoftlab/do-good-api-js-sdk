const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const SOURCE_PATH = path.resolve(__dirname, 'src');
const DIST_PATH = path.resolve(__dirname, 'dist');

module.exports = {
    entry: SOURCE_PATH,
    output: {
        path: DIST_PATH
    },
    resolve: {
        extensions: ['.ts', '.js']
    },
    mode: process.env.NODE_ENV,
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'awesome-typescript-loader'
            }
        ]
    },
    devServer: {
        port: 8000
    },
    plugins: [
        new HtmlWebpackPlugin()
    ]
};
